package resutils

import (
	"context"
	"net/url"

	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// Interfaccia che estende Store e QueryStore, utile per definire uno store che può
// funzionare sia da Store che da QueryStore.
type CompleteStore[T WithID] interface {
	store.Store[T]
	store.QueryStore[T]

	GetTotal(q url.Values) (int64, error)

	ReadCtx(ctx context.Context, id string) store.ReadTxn[T]

	// Apre una transazione in scrittura sulla risorsa, utilizzando il context fornito per
	// la cancellazione. Per indicare un initiator della modifica, dal quale poi Resgate
	// produrrà la proprietà External degli eventi relativi, aggiungere ai Value del context
	// il CID della connessione (recuperata dalla Request) con chiave resutils.CID{}
	WriteCtx(ctx context.Context, id string) store.WriteTxn[T]
}

// type WithID = store.WithID
type WithID interface {
	GetID() string
	// SetID(string) error
}
