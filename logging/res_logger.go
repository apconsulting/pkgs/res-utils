package logging

import (
	"fmt"

	"log/slog"

	"gitlab.com/apconsulting/pkgs/res/go-res/logger"
)

type ResLogger struct {
	sl *slog.Logger
}

func NewResLogger(sl *slog.Logger) *ResLogger {
	return &ResLogger{
		sl: sl,
	}
}

var _ logger.Logger = &ResLogger{}

func (rl *ResLogger) Infof(format string, v ...any) {
	fmt.Printf("INFO: "+format, v...)
	fmt.Println()
}

func (rl *ResLogger) Errorf(format string, v ...any) {
	fmt.Printf("ERROR: "+format, v...)
	fmt.Println()
}

func (rl *ResLogger) Tracef(format string, v ...any) {
	fmt.Printf("TRACE: "+format, v...)
	fmt.Println()
}
