package logging

import (
	"bytes"
	"context"
	"fmt"
	"strings"

	"log/slog"

	"github.com/nats-io/nats.go"
)

const (
	NATS_SUBJECTS_ROOT = "log"

	LevelFatal slog.Level = 12
)

type NatsHandler struct {
	nc         *nats.Conn
	svcName    string
	stdoutEcho bool

	buffer *bytes.Buffer

	slog.JSONHandler
}

var _ slog.Handler = &NatsHandler{}

func NewNatsHandler(nc *nats.Conn, service string, stdoutEcho bool) *NatsHandler {
	newHandler := NatsHandler{
		nc:         nc,
		svcName:    service,
		stdoutEcho: stdoutEcho,
	}

	newHandler.buffer = bytes.NewBuffer([]byte{})
	newHandler.JSONHandler = *slog.NewJSONHandler(newHandler.buffer, &slog.HandlerOptions{
		AddSource: true,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.LevelKey {
				// str := func(base string, val slog.Level) string {
				// 	if val == 0 {
				// 		return base
				// 	}
				// 	return fmt.Sprintf("%s%+d", base, val)
				// }

				// level := a.Value.Any().(slog.Level)
				// if level >= LevelFatal {
				// 	a.Value = slog.StringValue(str("FATAL", level-LevelFatal))
				// }
				a.Value = slog.IntValue(int(a.Value.Any().(slog.Level)))
			}
			return a
		},
	})

	return &newHandler
}

func (h *NatsHandler) Handle(ctx context.Context, r slog.Record) error {
	err := h.JSONHandler.Handle(ctx, r)
	if err != nil {
		return err
	}

	defer h.buffer.Reset()

	buf := h.buffer.Bytes()
	if h.stdoutEcho {
		fmt.Printf("LOG: %s\n", strings.TrimSpace(string(buf)))
	}

	return h.nc.Publish(strings.Join([]string{NATS_SUBJECTS_ROOT, h.svcName}, "."), buf)

}
