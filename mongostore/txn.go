package mongostore

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"sync/atomic"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var ErrUnmodifiable = errors.New("value not modifiable")

// ReadTxn
type MongoReadTxn[T resutils.WithID] struct {
	collection  *mongo.Collection
	aggregation bson.A

	ctx      context.Context
	cancFunc context.CancelFunc

	cached   bool
	notFound bool
	value    T

	createBeforeGet func(id string) (T, error)

	idFieldName string
	idFieldType byte

	id string

	closed atomic.Bool
}

// Assert implementation
var _ store.ReadTxn[resutils.WithID] = &MongoReadTxn[resutils.WithID]{}

func (tx *MongoReadTxn[T]) Close() error {
	if tx.closed.Swap(true) {
		return resutils.ErrTxnClosed
	}

	defer tx.cancFunc()
	return nil
}

func (tx *MongoReadTxn[T]) Exists() bool {
	// Carico la cache e vedo che mi dice
	_, err := tx.Value()
	return err == nil
}

func (tx *MongoReadTxn[T]) ID() string {
	return tx.id
}

func (tx *MongoReadTxn[T]) Value() (T, error) {
	var v T
	if tx.id == "" {
		return v, store.ErrNotFound
	}

	if tx.cached {
		if tx.notFound {
			return v, store.ErrNotFound
		}

		return tx.value, nil
	}

	var result T

	// Converto l'id della transaction nel tipo corretto dato che Mongo lo pretende
	id, err := tx.getTypedID()
	if err != nil {
		return v, fmt.Errorf("wrong idFieldType: %w", err)
	}

	// TODO: Boh sta roba me par un poccio... Ormai l'ho scritta e la lascio così ma
	// subentrano problemi per quanto riguarda la gestione dei modelli, che cambiano tra
	// lettura e scrittura dei dati. Potrebbe servire, dato che potenzialmente non è detto
	// che una semplice FindOne copra tutti i casi possibili, però è da considerare con
	// attenzione il caso d'uso.
	if tx.aggregation != nil {
		aggr := bson.A{
			bson.D{{Key: "$match", Value: bson.D{{Key: tx.idFieldName, Value: id}}}},
		}
		aggr = append(aggr, tx.aggregation...)

		var c *mongo.Cursor
		c, err = tx.collection.Aggregate(tx.ctx, aggr, nil)

		if err == nil {
			aggrRes := make([]T, 0)
			c.All(tx.ctx, &aggrRes)

			if len(aggrRes) != 1 {
				return v, fmt.Errorf("wrong number of results from aggregate: %d", len(aggrRes))
			}

			result = aggrRes[0]
		}
	} else {
		err = tx.collection.FindOne(
			tx.ctx,
			bson.D{{Key: tx.idFieldName, Value: id}},
		).Decode(&result)
	}

	if err != nil {
		if err != mongo.ErrNoDocuments {
			return v, err
		}

		if tx.createBeforeGet == nil {
			tx.cached = true
			tx.notFound = true

			return v, store.ErrNotFound
		}

		result, err = tx.createBeforeGet(tx.id)
		if err != nil {
			return v, err
		}

		_, err = tx.collection.InsertOne(tx.ctx, result)

		if err != nil {
			return v, err
		}
	}

	tx.value = result
	tx.cached = true

	return tx.value, nil
}

// WriteTxn
type MongoWriteTxn[T resutils.WithID] struct {
	*MongoReadTxn[T]

	// Se true, vado in errore automaticamente su ogni operazione che modifica i dati. Serve
	// nel caso il dato provenga da un dataSet (quindi non modificabile), dato che non posso
	// ritornare un errore in creazione della txn dallo store, devo usare sto potaccio per
	// andare in errore piu avanti. Grazie Jirenius.
	// TODO: Avendo preso per mano go-res ormai potrebbe essere il momento giusto di
	// cambiare sta cosa...
	unmodifiable bool

	onChangeFunc func(string, store.Nullable[T], store.Nullable[T])
	idFactory    func(*T) error
}

// Assert implementation
var _ store.WriteTxn[resutils.WithID] = &MongoWriteTxn[resutils.WithID]{}

func (tx *MongoWriteTxn[T]) Create(val *T) error {
	if tx.unmodifiable {
		return fmt.Errorf("can't create on an unmodifiable transaction: %w", store.ErrDuplicate)
	}

	// assertedVal, ok := val.(T)
	// if !ok {
	// 	return resutils.ErrAssertionFailed
	// }

	if tx.id == "" {
		// newID, err := tx.idFactory(assertedVal)
		err := tx.idFactory(val)
		if err != nil {
			return err
		}

		tx.id = (*val).GetID()
	}

	_, err := tx.Value()
	if err == nil {
		return store.ErrDuplicate
	}

	_, err = tx.collection.InsertOne(
		tx.ctx,
		val,
	)

	if err != nil {
		return err
	}

	tx.value = *val
	tx.cached = true

	tx.onChangeFunc((*val).GetID(), store.Nullable[T]{}, store.AsNullable(tx.value))

	return nil
}

func (tx *MongoWriteTxn[T]) Delete() error {
	if tx.unmodifiable {
		return ErrUnmodifiable
	}

	if tx.id == "" {
		return store.ErrNotFound
	}

	previousVal, err := tx.Value()
	if err != nil {
		return err
	}

	oid, err := tx.getTypedID()
	if err != nil {
		return fmt.Errorf("wrong idFieldType: %w", err)
	}

	_, err = tx.collection.DeleteOne(
		tx.ctx,
		bson.D{{Key: tx.idFieldName, Value: oid}},
	)

	if err != nil {
		return err
	}

	tx.cached = false

	tx.onChangeFunc(tx.id, store.AsNullable(previousVal), store.Nullable[T]{})

	return nil
}

func (tx *MongoWriteTxn[T]) Update(val T) error {
	if tx.unmodifiable {
		return ErrUnmodifiable
	}

	if tx.id == "" {
		return store.ErrNotFound
	}

	// b, err := json.Marshal(val)
	// if err != nil {
	// 	return err
	// }

	// assertedVal := make(map[string]any)
	// err = json.Unmarshal(b, &assertedVal)
	// if err != nil {
	// 	return err
	// }
	// if !ok {
	// 	return resutils.ErrAssertionFailed
	// }

	previousVal, err := tx.Value()
	if err != nil {
		return err
	}

	id, err := tx.getTypedID()
	if err != nil {
		return fmt.Errorf("wrong idFieldType: %w", err)
	}

	// Non passo l'idField a MongoDB, perché è chiave e quindi non va modificato mai.
	// Eventuali operazioni che "aggiornano" l'id vanno trattate come una eliminazione con
	// conseguente creazione di un nuovo elemento con il nuovo id, così anche la gestione
	// degli eventi di Res è rispettata.
	// delete(assertedVal, tx.idFieldName)
	err = setIDFieldToZero(&val)
	if err != nil {
		return fmt.Errorf("can't empty idField: %w", err)
	}

	_, err = tx.collection.UpdateOne(
		tx.ctx,
		bson.D{{Key: tx.idFieldName, Value: id}},
		bson.D{{Key: "$set", Value: val}})
	if err != nil {
		return err
	}

	// Refresho la cache andando a DB
	tx.cached = false
	newVal, err := tx.Value()
	if err != nil {
		return err
	}

	tx.onChangeFunc(tx.id, store.AsNullable(previousVal), store.AsNullable(newVal))

	return nil
}

func (tx *MongoWriteTxn[T]) UpdatePartial(v map[string]any) error {
	if tx.unmodifiable {
		return ErrUnmodifiable
	}

	if tx.id == "" {
		return store.ErrNotFound
	}

	val, err := tx.Value()
	if err != nil {
		return err
	}

	// Passo da una rappresentazione json per clonare il valore ottenuto da tx.Value(), in
	// quanto se il modello dovesse contenere dei puntatori, effettuare una Unmarshal
	// direttamente sopra il valore proveniente da tx.Value() sostituisce il valore SIA
	// nella variabile locale che nell'originale (entrambe puntano alla stessa zona di
	// memoria), compromettendo la capacità di rilevare le differenze tra i due modelli
	var prev T

	prevJS, err := json.Marshal(val)
	if err != nil {
		return err
	}

	err = json.Unmarshal(prevJS, &prev)
	if err != nil {
		return err
	}

	// Passo dal json per poter "mergiare" i campi dell'update con il valore precedente
	js, err := json.Marshal(v)
	if err != nil {
		return err
	}

	err = json.Unmarshal(js, &prev)
	if err != nil {
		return err
	}

	return tx.Update(prev)
}

func (tx *MongoReadTxn[T]) getTypedID() (any, error) {
	switch tx.idFieldType {
	case TypeObjectID:
		return primitive.ObjectIDFromHex(tx.id)
	case TypeInt:
		return strconv.Atoi(tx.id)
	default:
		return tx.id, nil
	}
}
