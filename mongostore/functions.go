package mongostore

import (
	"errors"
	"reflect"
	"strings"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	TypeObjectID byte = iota + 1
	TypeInt
	TypeString
)

var (
	ErrNotAStruct           = errors.New("variable must be struct, pointer to struct or interface boxing a struct")
	ErrMalformedTag         = errors.New("malformed mongo tag")
	ErrUnsupportedFieldType = errors.New("mongo tagged field must be int, string, primitive.ObjectID or a pointer to those")
	ErrMongoTagNotFound     = errors.New("no field of the supplied struct type has a mongo tag")
)

// Esegue la funzione fn sul primo campo taggato mongo:"pkey" trovato, scendendo nelle
// eventuali strutture embeddate e rispettando l'ordine con cui sono dichiarati i campi
// della struttura. Ritorna un errore se inValue non è una struct, un puntatore a struct o
// un'interfaccia il cui valore dinamico è una struct
func runFuncOnIDField(val reflect.Value, fn func(field reflect.StructField, fieldValue reflect.Value) error) error {
	inType := val.Type()
	elemVal := val

	for inType.Kind() == reflect.Pointer || inType.Kind() == reflect.Interface {
		inType = inType.Elem()
		if elemVal.IsNil() {
			elemVal = reflect.New(inType).Elem()
		} else {
			elemVal = elemVal.Elem()
		}
	}

	if inType.Kind() != reflect.Struct {
		return ErrNotAStruct
	}

	for i := 0; i < inType.NumField(); i++ {
		field := inType.Field(i)

		if field.Anonymous && field.IsExported() {
			// Scende ricorsivamente nelle strutture embeddate
			err := runFuncOnIDField(elemVal.Field(i), fn)
			if err == nil || !errors.Is(err, ErrNotAStruct) && !errors.Is(err, ErrMongoTagNotFound) {
				return err
			}

			continue
		}

		if tag, ok := field.Tag.Lookup("mongo"); ok {
			// Tag "mongo" trovato. Inizio a parsarlo
			theresMore := true

			for theresMore {
				var opt, remaining string
				opt, remaining, theresMore = strings.Cut(tag, ",")

				// Prevedo già la possibilità di avere varie "opzioni" per il tag mongo. Per
				// adesso c'è solo "pkey"
				switch opt {
				case "pkey":
					// "pkey": indica che il field è l'ID da usare nelle query MongoDB
					// v := elemVal.Field(i)

					return fn(field, elemVal.Field(i))
				}

				tag = remaining
			}

			return ErrMalformedTag
		}
	}

	return ErrMongoTagNotFound
}

// Estrae le informazioni sul campo da considerare come ID per le query su MongoDB. Dato
// un valore, che dev'essere una struct, puntatore a struct o interfaccia il cui valore
// dinamico è struct, controlla i vari field cercandone uno con tag `mongo:"pkey"`. Una
// volta trovato, ne estrae il nome dall'eventuale tag "bson" (se presente) e il tipo.
//
// Se la struttura fornita embedda altre strutture, la funzione scende ricorsivamente.
//
// La ricerca si interrompe al primo field con tag "mongo" trovato, senza verificare che
// ve ne siano altri (cosa importante da ricordare specialmente nel caso di strutture
// embeddate)
func extractIDField[T any](v T) (bsonFieldName string, idType byte, err error) {
	return extractIDFieldFromValue(reflect.ValueOf(v))
}

func extractIDFieldFromValue(inValue reflect.Value) (string, byte, error) {
	var bsonFieldName string
	var idType byte

	err := runFuncOnIDField(inValue, func(field reflect.StructField, fieldValue reflect.Value) error {
		fieldType := field.Type
		if fieldType.Kind() == reflect.Pointer {
			fieldType = fieldType.Elem()
		}

		if bsonTag, ok := field.Tag.Lookup("bson"); ok {
			// Tag "bson" trovato, recupero il nome del campo
			bsonFieldName, _, _ = strings.Cut(bsonTag, ",")
		} else {
			// altrimenti uso il nome del field stesso
			bsonFieldName = field.Name
		}

		if fieldType == reflect.TypeOf(primitive.ObjectID{}) {
			idType = TypeObjectID
		} else {
			switch fieldType.Kind() {
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
				reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
				idType = TypeInt
			case reflect.String:
				idType = TypeString
			default:
				return ErrUnsupportedFieldType
			}
		}

		return nil
	})

	return bsonFieldName, idType, err
}

func setIDFieldToNewObjectID[T resutils.WithID](v *T) error {
	return setIDField(v, reflect.ValueOf(primitive.NewObjectID()))
}

func setIDFieldToZero[T any](v *T) error {
	return setIDField(v, reflect.Value{})
}

// Sets the field tagged with mongo:"pkey" to the specified idVal. If !idVal.IsValid(),
// sets the field to its zero value
func setIDField[T any](v *T, idVal reflect.Value) error {
	// Panic if v is not dereferenceable (ie: nil pointer)
	_ = *v

	return runFuncOnIDField(reflect.ValueOf(v), func(field reflect.StructField, fieldValue reflect.Value) error {
		if !idVal.IsValid() {
			fieldValue.SetZero()
		} else {
			fieldValue.Set(idVal)
		}
		return nil
	})
}
