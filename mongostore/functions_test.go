package mongostore

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func TestExtractIDField(t *testing.T) {
	t.Log("Test1")

	var testVar1 struct {
		CampoID   string `mongo:"pkey"`
		ValoreInt int
		ValoreStr int
	}

	idField, idType, err := extractIDField(testVar1)
	assert.Equal(t, nil, err, "found error")
	assert.Equal(t, "CampoID", idField, "wrong idField name")
	assert.Equal(t, TypeString, idType, "wrong idType")

	t.Log("Test2")

	var testVar2 struct {
		CampoID int `mongo:"pkey" bson:"campo_id"`
	}

	idField, idType, err = extractIDField(testVar2)
	assert.Equal(t, nil, err, "found error")
	assert.Equal(t, "campo_id", idField, "wrong idField name")
	assert.Equal(t, TypeInt, idType, "wrong idType")

	t.Log("Test3")

	var testVar3 struct {
		CampoID int `mongo:"pkeyy" bson:"campo_id"`
	}

	_, _, err = extractIDField(testVar3)
	assert.Equal(t, ErrMalformedTag, err, "wrong error returned")

	t.Log("Test4")

	var testVar4 struct {
		CampoID primitive.ObjectID `mongo:"pkey" bson:"campo_oid"`
	}

	idField, idType, err = extractIDField(testVar4)
	assert.Equal(t, nil, err, "found error")
	assert.Equal(t, "campo_oid", idField, "wrong idField name")
	assert.Equal(t, TypeObjectID, idType, "wrong idType")

	t.Log("Test5")

	var testVar5 struct {
		CampoID int `bson:"campo_id"`
	}

	_, _, err = extractIDField(testVar5)
	assert.Equal(t, ErrMongoTagNotFound, err, "should have unexpected error")

	t.Log("Test6")

	testVar6 := "notAStruct"

	_, _, err = extractIDField(testVar6)
	assert.Equal(t, ErrNotAStruct, err, "should have unexpected error")

	t.Log("Test7")

	var testVar7 struct {
		CampoID bool `mongo:"pkey" bson:"campo_id"`
	}

	_, _, err = extractIDField(testVar7)
	assert.Equal(t, ErrUnsupportedFieldType, err, "should have unexpected error")

	t.Log("Test8")

	var testVar8 struct {
		CampoID *string `mongo:"pkey" bson:"campo_oid"`
	}

	idField, idType, err = extractIDField(testVar8)
	assert.Equal(t, nil, err, "unexpected error")
	assert.Equal(t, "campo_oid", idField, "wrong idField name")
	assert.Equal(t, TypeString, idType, "wrong idType")

	t.Log("Test9")

	type EmbeddedPrimitive int
	type DeepEmbedded struct {
		EmbeddedPrimitive
		CampoID primitive.ObjectID `mongo:"pkey" bson:"campo_deep"`
	}
	type Embedded struct {
		CampoExtra int `bson:"campo_extra"`
		DeepEmbedded
		CampoID int `mongo:"pkey" bson:"campo_embedded"`
	}
	var testVar9 struct {
		Embedded
		CampoID string `mongo:"pkey" bson:"campo_esterno"`
	}

	idField, idType, err = extractIDField(testVar9)
	assert.Equal(t, nil, err, "unexpected error")
	assert.Equal(t, "campo_deep", idField, "wrong idField name")
	assert.Equal(t, TypeObjectID, idType, "wrong idType")

	t.Log("Test10")

	var testVar10 struct {
		CampoID string `mongo:"pkey" bson:"campo_esterno"`
		Embedded
	}

	idField, idType, err = extractIDField(testVar10)
	assert.Equal(t, nil, err, "unexpected error")
	assert.Equal(t, "campo_esterno", idField, "wrong idField name")
	assert.Equal(t, TypeString, idType, "wrong idType")
}

func TestSetIDField(t *testing.T) {
	t.Log("Test panic")

	var hasPanickedCorrectly bool
	var err error
	func() {
		defer func() {
			hasPanickedCorrectly = recover().(error).Error() == "runtime error: invalid memory address or nil pointer dereference"
		}()

		err = setIDField[any](nil, reflect.Value{})
	}()

	assert.Equal(t, true, hasPanickedCorrectly, "no panic with nil pointer")
	assert.Equal(t, nil, err, "no error expected when panicking")

	t.Log("Test1")

	var testVar1 struct {
		CampoID   string `mongo:"pkey"`
		ValoreInt int
		ValoreStr int
	}

	err = setIDField(&testVar1, reflect.ValueOf("nuovoID"))
	assert.Equal(t, nil, err, "found error")
	assert.Equal(t, "nuovoID", testVar1.CampoID, "wrong idField value")

	t.Log("Test9")

	type EmbeddedPrimitive int
	type DeepEmbedded struct {
		EmbeddedPrimitive
		CampoID primitive.ObjectID `mongo:"pkey" bson:"campo_deep"`
	}
	type Embedded struct {
		CampoExtra int `bson:"campo_extra"`
		DeepEmbedded
		CampoID int `mongo:"pkey" bson:"campo_embedded"`
	}
	var testVar9 struct {
		Embedded
		CampoID string `mongo:"pkey" bson:"campo_esterno"`
	}

	newOID := primitive.NewObjectID()
	err = setIDField(&testVar9, reflect.ValueOf(newOID))
	assert.Equal(t, nil, err, "found error")
	assert.Equal(t, "", testVar9.CampoID, "wrong IDField changed")
	assert.Equal(t, newOID, testVar9.Embedded.DeepEmbedded.CampoID, "wrong idField value")

	t.Log("Test10")

	var testVar10 struct {
		CampoID string `mongo:"pkey" bson:"campo_esterno"`
		Embedded
	}

	prevEmbeddedID := testVar10.Embedded.DeepEmbedded.CampoID
	err = setIDField(&testVar10, reflect.ValueOf("nuovoID"))
	assert.Equal(t, nil, err, "found error")
	assert.Equal(t, prevEmbeddedID, testVar10.Embedded.DeepEmbedded.CampoID, "wrong IDField changed")
	assert.Equal(t, "nuovoID", testVar10.CampoID, "wrong idField value")

}
