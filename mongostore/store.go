package mongostore

import (
	"context"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"sync"

	"github.com/mitchellh/hashstructure/v2"
	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
// DB_TIMEOUT = 3 * time.Second
)

type MongoStore[T resutils.WithID] struct {
	sync.RWMutex

	collection *mongo.Collection

	idFieldName string
	idFieldType byte
	idFactory   func(*T) error

	// Callbacks registrate tramite l'interfaccia store.Store
	onChangeCallbacks []func(string, store.Nullable[T], store.Nullable[T], any)

	// Callbacks registrate tramite l'interfaccia store.QueryStore
	onQueryChangeCallbacks []func(store.QueryChange[T])

	queryFunc   func(url.Values) ([]string, error)
	queryParser func(url.Values) (bson.D, error)

	// Se != nil, quando leggo il dato da DB e non lo trovo, uso questa factory per generare
	// il nuovo elemento e lo salvo contestualmente in DB.
	createBeforeGet func(id string) (T, error)

	dataSet map[string]T
}

// Assert implementations
var _ resutils.CompleteStore[resutils.WithID] = (*MongoStore[resutils.WithID])(nil)

func NewMongoStore[T resutils.WithID](coll *mongo.Collection, opts ...storeOption[T]) (*MongoStore[T], error) {
	// newT := *new(T)
	var newT T
	idFieldName, idFieldType, err := extractIDField(newT)

	if err != nil && err != ErrMongoTagNotFound && err != ErrNotAStruct {
		return nil, err
	}

	newStore := &MongoStore[T]{
		collection:  coll,
		idFieldName: idFieldName,
		idFieldType: idFieldType,
	}

	newStore.OnChange(func(id string, before, after store.Nullable[T], info interface{}) {
		// hashstructure.Hash(js, hashstructure.FormatV2, nil)
		beforeHash, err := hashstructure.Hash(before, hashstructure.FormatV2, nil)
		if err != nil {
			return
		}
		afterHash, err := hashstructure.Hash(after, hashstructure.FormatV2, nil)
		if err != nil {
			return
		}

		if beforeHash == afterHash {
			return
		}

		for _, queryCB := range newStore.onQueryChangeCallbacks {
			queryCB(&resutils.QueryChange[T]{
				Id:        id,
				BeforeVal: before,
				AfterVal:  after,
			})
		}
	})

	// Setto una factory di default in grado di generare un nuovo ObjectID. Per tutti gli
	// altri tipi di ID vado in errore, dato che la factory dev'essere fornita da fuori.
	newStore.idFactory = func(val *T) error {
		if idFieldType == TypeObjectID {
			return setIDFieldToNewObjectID(val)
			// return primitive.NewObjectID().Hex(), nil
		}

		return resutils.ErrNoIDFactorySpecified
	}

	for _, o := range opts {
		o.Apply(newStore)
	}

	if newStore.idFieldName == "" || newStore.idFieldType == 0 {
		return nil, fmt.Errorf("missing idField specifications")
	}

	if newStore.queryFunc == nil {
		newStore.queryFunc = defaultQueryFuncFactory[T](newStore)
	}

	return newStore, nil
}

// Registra una callback sugli eventi riguardanti un singolo Model
func (s *MongoStore[T]) OnChange(cb func(string, store.Nullable[T], store.Nullable[T], any)) {
	s.onChangeCallbacks = append(s.onChangeCallbacks, cb)
}

// Registra una callback che gestice un possibile cambiamento alla query
func (s *MongoStore[T]) OnQueryChange(cb func(store.QueryChange[T])) {
	s.onQueryChangeCallbacks = append(s.onQueryChangeCallbacks, cb)
}

func (s *MongoStore[T]) Query(q url.Values) ([]string, error) {
	return s.queryFunc(q)
}

func (s *MongoStore[T]) Read(id string) store.ReadTxn[T] {
	return s.ReadCtx(context.Background(), id)
}

func (s *MongoStore[T]) ReadCtx(ctx context.Context, id string) store.ReadTxn[T] {
	s.RLock()

	ctx, cancFunc := context.WithCancel(ctx)

	go func() {
		<-ctx.Done()
		s.RUnlock()
	}()

	return s.newReadTxn(ctx, id, cancFunc)
}

func (s *MongoStore[T]) newReadTxn(ctx context.Context, id string, cancFunc func()) *MongoReadTxn[T] {
	txn := &MongoReadTxn[T]{
		id:              id,
		collection:      s.collection,
		idFieldName:     s.idFieldName,
		idFieldType:     s.idFieldType,
		cancFunc:        cancFunc,
		createBeforeGet: s.createBeforeGet,

		ctx: ctx,
	}

	if val, ok := s.dataSet[id]; ok {
		txn.value = val
		txn.cached = true
	}

	return txn
}

func (s *MongoStore[T]) Write(id string) store.WriteTxn[T] {
	return s.WriteCtx(context.Background(), id)
}

func (s *MongoStore[T]) WriteCtx(ctx context.Context, id string) store.WriteTxn[T] {
	s.Lock()

	ctx, cancFunc := context.WithCancel(ctx)

	go func() {
		<-ctx.Done()
		s.Unlock()
	}()

	var info any = nil

	cid := ctx.Value(resutils.CID{})
	if cid != nil {
		info = resutils.EventInfo{
			Initiator: cid.(string),
		}
	}

	txn := &MongoWriteTxn[T]{
		MongoReadTxn: s.newReadTxn(ctx, id, cancFunc),

		idFactory: s.idFactory,
		onChangeFunc: func(id string, before, after store.Nullable[T]) {
			for _, changeCB := range s.onChangeCallbacks {
				changeCB(id, before, after, info)
			}
		},
	}

	if _, ok := s.dataSet[id]; ok {
		// Grazie Jirenius per questo potaccio!
		txn.unmodifiable = true
	}

	return txn
}

func (s *MongoStore[T]) GetTotal(q url.Values) (int64, error) {
	return defaultGetTotal(s)(q)
}

func defaultGetTotal[T resutils.WithID](st *MongoStore[T]) func(url.Values) (int64, error) {
	return func(query url.Values) (int64, error) {
		filter := bson.D{}

		if st.queryParser != nil {
			var err error
			filter, err = st.queryParser(query)

			if err != nil {
				return 0, err
			}
		}

		return st.collection.CountDocuments(context.TODO(), filter)
	}
}

func defaultQueryFuncFactory[T resutils.WithID](st *MongoStore[T]) func(url.Values) ([]string, error) {
	return func(query url.Values) ([]string, error) {
		opt := options.FindOptions{}

		fromStr := query.Get("from")
		if fromStr != "" {
			from, _ := strconv.Atoi(fromStr)
			opt.SetSkip(int64(from))
		}

		limitStr := query.Get("limit")
		if limitStr != "" {
			limit, _ := strconv.Atoi(limitStr)
			opt.SetLimit(int64(limit))
		}

		sortStr := query.Get("sort")
		if sortStr != "" {
			splits := strings.Split(sortStr, ",")

			sort := bson.D{}

			for _, split := range splits {
				ord := 1

				if len(split) > 0 {
					if split[0] == '-' {
						split = split[1:]
						ord = -1
					}

					// Riverifico perché split è cambiato se split[0] == '-'
					if len(split) > 0 {
						sort = append(sort, bson.E{Key: split, Value: ord})
					}
				}
			}

			opt.SetSort(sort)
		}

		filter := bson.D{}

		if st.queryParser != nil {
			var err error
			filter, err = st.queryParser(query)
			if err != nil {
				return nil, err
			}
		}

		c, err := st.collection.Find(context.TODO(),
			filter,
			&opt)
		if err != nil {
			return nil, err
		}

		ids := make([]string, 0)
		for c.Next(context.TODO()) {
			// var elem = *new(T)
			var elem T
			err := c.Decode(&elem)

			if err != nil {
				return nil, err
			}

			ids = append(ids, elem.GetID())
		}

		return ids, nil
	}
}
