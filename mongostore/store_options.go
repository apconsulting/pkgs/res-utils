package mongostore

import (
	"net/url"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"go.mongodb.org/mongo-driver/bson"
)

/*

In questo playground esemplifico il problema relativo all'inferenza per cui è necessario
specificare il tipo T per le functional options che non hanno parametri di tipo T
(in quel caso l'inferenza funziona sul tipo dei parametri stessi)

// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type Store[T any] struct {
	value T
}

func NewStore[T any]() Store[T] {
	return Store[T]{}
}

func main() {
	var myStore Store[int]

	var NewStoreInt func() Store[int] = NewStore // this one infers correctly
	myStore = NewStoreInt()

	myStore = NewStore[int]() // Must provide the type argument,
	myStore = NewStore() // error: cannot infer T (prog.go:11:15)

	fmt.Println(NewStoreInt, myStore)
}

*/

type storeOption[T resutils.WithID] interface {
	Apply(*MongoStore[T])
}

type storeOptionFunc[T resutils.WithID] func(st *MongoStore[T])

func (o storeOptionFunc[T]) Apply(st *MongoStore[T]) {
	o(st)
}

func WithIDField[T resutils.WithID](idFieldName string, idFieldType byte) storeOption[T] {
	return storeOptionFunc[T](func(st *MongoStore[T]) {
		st.idFieldName = idFieldName
		st.idFieldType = idFieldType
	})
}

// Specifica una nuova funzione per la generazione del nuovo ID in fase di creazione nuovo
// elemento
func WithIDFactory[T resutils.WithID](factory func(*T) error) storeOption[T] {
	return storeOptionFunc[T](func(st *MongoStore[T]) {
		st.idFactory = factory
		// st.idFactory = func(newVal resutils.WithID) (string, error) {
		// 	return factory(newVal.(T))
		// }
	})
}

func WithQueryParser[T resutils.WithID](f func(url.Values) (bson.D, error)) storeOption[T] {
	return storeOptionFunc[T](func(st *MongoStore[T]) {
		st.queryParser = f
	})
}

func WithQueryFunc[T resutils.WithID](newQueryFunc func(url.Values) ([]string, error)) storeOption[T] {
	return storeOptionFunc[T](func(st *MongoStore[T]) {
		st.queryFunc = newQueryFunc
	})
}

// Se durante una get non trovo il valore in database, utilizzo la factory fornita per
// generarne uno nuovo a partire dall'ID (unico dato conosciuto) e contestualmente lo
// inserisco in database. Da li in poi lo tratto come se fosse sempre esistito.
func WithCreateBeforeGet[T resutils.WithID](f func(id string) (T, error)) storeOption[T] {
	return storeOptionFunc[T](func(st *MongoStore[T]) {
		st.createBeforeGet = f
	})
}

// Aggiunge allo store un dataset di valori di default che non può essere modificato in
// alcun modo, utile per quei valori che non provengono da db ma devono essere gestiti in
// maniera equivalente (ad es. l'utente admin in ms_auth)
func WithDataSet[T resutils.WithID](data map[string]T) storeOption[T] {
	return storeOptionFunc[T](func(st *MongoStore[T]) {
		st.dataSet = data
	})
}
