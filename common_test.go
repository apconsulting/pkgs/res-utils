package resutils

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/apconsulting/pkgs/res/go-res"
)

type Test struct {
	In
	CampoID      *int `json:"id"`
	CampoRid     bool `res:"rid:ms_easyapi.lastra_collection.$(CampoID,base64).ancora.$(CampoID)" json:"campo_rid"`
	CampoOmitPtr bool `res:"rid:ms_easyapi.lastra_collection.$(CampoID,base64,omitempty).ancora.$(CampoID)" json:"campo_omit"`
	// ElencoRids   []string `res:"rid:ms_auth.altrotest.$id"`
	CampoData    []string `json:"campo_data" res:"data:campo_res_data"`
	CampoStringa string   `json:"campo_stringa"`
	CampoNo      string   `json:"-"`
	CampoNormale string
	CampoOmit    string `res:"rid:ms_easyapi.lastra_collection.$(CampoOmit,omitempty)"`
}

type In struct {
	CampoIn string
}

func TestResParse(t *testing.T) {
	var id int
	testVar := Test{
		CampoID:  &id,
		CampoRid: true,
		CampoData: []string{
			"Riga1",
			"Riga2",
		},
		CampoStringa: "Testo",
		CampoNormale: "niente",
		CampoOmit:    "DoNotSkipMe",
	}

	//fmt.Println(ResParse(testVar))
	spew.Dump(testVar)
	fmt.Println("---------------------------")

	result, err := ResParse(testVar)
	if err != nil {
		t.Error(err)
	}

	js, _ := json.MarshalIndent(result, "", "  ")
	fmt.Println(string(js))
	//spew.Dump(result)
}

func TestResUnparse(t *testing.T) {
	var id int = 45
	testVar := Test{
		CampoID:  &id,
		CampoRid: true,
		CampoData: []string{
			"Riga1",
			"Riga2",
		},
		CampoStringa: "Testo",
		CampoNormale: "niente",
	}

	result, err := ResParse(testVar)
	if err != nil {
		t.Error(err)
	}

	spew.Dump(result)

	unparsed := ResUnparse(result)

	spew.Dump(unparsed)

	if reflect.TypeOf(unparsed["camp_res_data"]) == reflect.TypeOf(res.DataValue{}) {
		t.Error("unparse fallito")
	}
}
