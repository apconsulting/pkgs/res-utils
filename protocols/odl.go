package protocols

import "gitlab.com/apconsulting/pkgs/res/go-res"

type ODLPayload struct {
	OdlID     string `json:"odl_id"`
	MachineID string `json:"machine_id"`

	Slabs []res.Ref `json:"slabs,omitempty"`
}
