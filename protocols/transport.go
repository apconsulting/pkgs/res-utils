package protocols

import "gitlab.com/apconsulting/pkgs/res/go-res"

type TransportPayload struct {
	TransportID string `json:"transport_id"`
	CraneID     string `json:"crane_id"`

	From string `json:"from"`

	Slabs []res.Ref `json:"slabs"`
}
