package handlers

import (
	"net/url"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

func StandardTotalHandler[T resutils.WithID](st resutils.CompleteStore[T], opts ...HandlerOption[T]) res.Option {
	tho := totalHandlerOptions[T]{
		qReqHandler: resutils.DefaultQueryRequestHandler,
	}

	for _, opt := range opts {
		opt.Apply((*handlerOptions[T])(&tho))
	}

	return res.OptionFunc(func(hnd *res.Handler) {
		hnd.Option(resutils.CheckLoggedIn)
		hnd.Option(res.OnRegister(func(service *res.Service, pattern res.Pattern, rh res.Handler) {
			// Quando registro questo handler sul Service, registro anche una OnQueryChange
			// sullo store, cosi quando cambiano i dati posso inviare un QueryEvent

			st.OnQueryChange(func(qc store.QueryChange[T]) {
				resource, err := service.Resource(string(pattern))
				if err != nil {
					panic(err)
				}

				resource.QueryEvent(func(qr res.QueryRequest) {
					if qr == nil {
						return
					}

					q, err := url.ParseQuery(qr.Query())
					if err != nil {
						qr.Error(err)
						return
					}

					// q, _, err = resutils.DefaultQueryRequestHandler(qr.ResourceName(),
					// qr.PathParams(), q)
					q, _, err = tho.qReqHandler(qr.ResourceName(), qr.PathParams(), q)
					if err != nil {
						qr.Error(err)
						return
					}

					tot, err := st.GetTotal(q)
					if err != nil {
						qr.Error(err)
						return
					}

					qr.Model(struct {
						Total int64 `json:"total"`
					}{
						Total: tot,
					})
				})
			})
		}))

		res.GetModel(func(mr res.ModelRequest) {
			q, err := url.ParseQuery(mr.Query())
			if err != nil {
				mr.Error(err)
				return
			}

			// q, normQuery, err := resutils.DefaultQueryRequestHandler(mr.ResourceName(),
			// mr.PathParams(), q)
			q, normQuery, err := tho.qReqHandler(mr.ResourceName(), mr.PathParams(), q)
			if err != nil {
				mr.Error(err)
				return
			}

			tot, err := st.GetTotal(q)
			if err != nil {
				mr.Error(err)
				return
			}

			mr.QueryModel(struct {
				Total int64 `json:"total"`
			}{
				Total: tot,
			}, normQuery)
		}).SetOption(hnd)

	})
}
