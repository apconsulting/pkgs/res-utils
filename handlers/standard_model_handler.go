package handlers

import (
	"context"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// Crea un handler "standard" usando lo store fornito e aggiungendo i metodo Set e Delete
// per il CRUD di base.
func StandardModelHandler[T resutils.WithID](mStore resutils.CompleteStore[T], opts ...HandlerOption[T]) res.Option {
	mho := modelHandlerOptions[T]{
		resType: res.Model,
		// accessHandler: resutils.CheckLoggedIn,
		stHandler: &store.Handler[T]{
			Store: mStore,
		},

		setMethod: func(cr res.CallRequest) {
			// Devo usare il tipo CID:
			// should not use built-in type string as key for value; define your own type to
			// avoid collisions (SA1029) go-staticcheck
			ctx := context.WithValue(context.Background(), resutils.CID{}, cr.CID())

			// Create a store write transaction.
			txn := mStore.WriteCtx(ctx, cr.PathParam("id"))
			defer txn.Close()

			obj := make(map[string]any)
			cr.ParseParams(&obj)

			err := txn.UpdatePartial(obj)
			if err != nil {
				cr.Error(err)
				return
			}

			// Send success response
			cr.OK(nil)
		},

		deleteMethod: func(cr res.CallRequest) {
			// Create a store write transaction
			txn := mStore.Write(cr.PathParam("id"))
			defer txn.Close()

			// Delete the customer from the store.
			// This will produce a query event for the customers query collection.
			if err := txn.Delete(); err != nil {
				cr.Error(err)
				return
			}

			// Send success response.
			cr.OK(nil)
		},

		extraMethods: make([]extraMethod, 0),
	}

	for _, o := range opts {
		o.Apply((*handlerOptions[T])(&mho))
	}

	return res.OptionFunc(func(hs *res.Handler) {
		mho.ApplyToHandler(hs)
	})
}
