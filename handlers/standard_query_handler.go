package handlers

import (
	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// Crea un handler "standard" che effettua il CRUD di base usando lo store fornito.
func StandardQueryHandler[T resutils.WithID](qStore resutils.CompleteStore[T], modelResPath string, opts ...HandlerOption[T]) res.Option {
	qho := queryHandlerOptions[T]{
		resType: res.Collection,
		stQHandler: &store.QueryHandler[T]{
			QueryStore:          qStore,
			QueryRequestHandler: resutils.DefaultQueryRequestHandler,
		},

		modelResPath: modelResPath,

		// newFunc: func() (T, error) {
		// 	return *new(T), nil
		// },

		extraMethods: make([]extraMethod, 0),
	}

	qho.createMethod = func(cr res.CallRequest) {
		// var newVal = *new(T)
		var newVal T
		cr.ParseParams(&newVal)

		if qho.beforeCreateFunc != nil {
			err := qho.beforeCreateFunc(&newVal)
			if err != nil {
				cr.Error(err)
				return
			}
		}

		txn := qStore.Write(newVal.GetID())
		defer txn.Close()

		// Add the customer to the store.
		// This will produce a query event for the customers query collection.
		if err := txn.Create(&newVal); err != nil {
			cr.Error(err)
			return
		}

		if qho.afterCreateFunc != nil {
			err := qho.afterCreateFunc(&newVal)
			if err != nil {
				cr.Error(err)
				return
			}
		}

		refs, err := qho.stQHandler.Transformer.TransformResult([]string{newVal.GetID()})
		if err != nil {
			cr.Error(err)
			return
		}

		// TODO: Se uso un transformer diverso da IDToRIDCollectionTransformer il valore di
		// ritorno potrebbe non essere un []res.Ref e salta tutto per aria. Gestire eventuali
		// altri tipi di dato, se serve.

		// switch refs := x.(type) {
		// case []res.Ref:
		cr.Resource(string(refs[0]))
		// default:
		// cr.Error(resutils.ErrAssertionFailed)
		// return
		// }
	}

	for _, o := range opts {
		o.Apply((*handlerOptions[T])(&qho))
	}

	return res.OptionFunc(func(hs *res.Handler) {
		qho.ApplyToHandler(hs)
	})
}
