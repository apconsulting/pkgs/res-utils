package handlers

import (
	"encoding/base64"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

type DefaultTransformer[T resutils.WithID] struct {
	beforeTransform func(string, T) (T, error)

	// transformFunc   func(id string, v any) (any, error)

	afterTransform func(string, map[string]any) (map[string]any, error)
}

var _ store.Transformer[resutils.WithID] = &DefaultTransformer[resutils.WithID]{}

func (h *DefaultTransformer[T]) RIDToID(rid res.Ref, pathParams map[string]string) string {
	return pathParams["id"]
}

// IDToRID transforms a customer ID used by the store to an external resource
// ID.
//
// The pattern, p, is the full pattern registered to the service (eg.
// "search.customer.$id") for this resource.
func (h *DefaultTransformer[T]) IDToRID(id string, v T, p res.Pattern) res.Ref {
	return res.Ref(p.ReplaceTag("id", id))
}

// Transform allows us to transform the stored customer model before sending it
// off to external clients. In this example, we do no transformation.
func (h *DefaultTransformer[T]) Transform(id string, v T) (any, error) {

	// assertedVal, ok := v.(resutils.WithID)

	// if !ok {
	// 	return nil, fmt.Errorf("DefaultTransformer.Transform: %w", resutils.ErrAssertionFailed)
	// }

	if h.beforeTransform != nil {
		var err error
		v, err = h.beforeTransform(id, v)
		if err != nil {
			return nil, err
		}
	}

	parsed, err := resutils.ResParse(v)
	if err != nil {
		return nil, err
	}

	if h.afterTransform != nil {
		parsed, err = h.afterTransform(id, parsed)
		if err != nil {
			return nil, err
		}
	}

	return parsed, err
}

// Base64ID Model Transformer
type Base64IDTransformer[T resutils.WithID] struct {
	DefaultTransformer[T]
}

var _ store.Transformer[resutils.WithID] = &Base64IDTransformer[resutils.WithID]{}

func (h *Base64IDTransformer[T]) RIDToID(rid res.Ref, pathParams map[string]string) string {
	decodedID, _ := base64.URLEncoding.DecodeString(pathParams["id"])
	return string(decodedID)
}

func (h *Base64IDTransformer[T]) IDToRID(id string, v T, p res.Pattern) res.Ref {
	return res.Ref(p.ReplaceTag("id", base64.URLEncoding.EncodeToString([]byte(id))))
}

// var _ store.QueryTransformer = &DefaultQueryTransformer{}

// type DefaultQueryTransformer struct {
// 	modelResPath string
// }

// func (t *DefaultQueryTransformer) TransformResult(v any) (any, error) {
// 	ids, ok := v.([]string)
// 	if !ok {
// 		return nil, fmt.Errorf("failed to transform results: expected value of type []string, but got %s", reflect.TypeOf(v))
// 	}
// 	refs := make([]res.Ref, len(ids))
// 	for i, id := range ids {
// 		refs[i] = res.Ref(strings.Join([]string{t.modelResPath, id}, "."))
// 	}
// 	return refs, nil
// }

// func (t *DefaultQueryTransformer) TransformEvents(evs []store.ResultEvent) ([]store.ResultEvent, error) {
// 	for i, ev := range evs {
// 		if ev.Name == "add" {
// 			id, ok := ev.Value.(string)
// 			if !ok {
// 				return nil, fmt.Errorf("failed to transform add event: expected value of type string, but got %s", reflect.TypeOf(ev.Value))
// 			}
// 			evs[i].Value = res.Ref(strings.Join([]string{t.modelResPath, id}, "."))
// 		}
// 	}
// 	return evs, nil
// }

/*type IDToRIDCollectionTransformer func(id string) res.Ref

// TransformResult transforms a slice of id strings into a slice of resource
// references.
func (t IDToRIDCollectionTransformer) TransformResult(v []string) ([]res.Ref, error) {
	ids := v
	// if !ok {
	// 	return nil, fmt.Errorf("failed to transform results: expected value of type []string, but got %s", reflect.TypeOf(v))
	// }
	refs := make([]res.Ref, len(ids))
	for i, id := range ids {
		refs[i] = res.Ref(t(id))
	}
	return refs, nil
}

// TransformEvents transforms events for a []string collection into events for a
// []res.Ref collection.
func (t IDToRIDCollectionTransformer) TransformEvents(evs []ResultEvent[string]) ([]ResultEvent[string], error) {
	for i, ev := range evs {
		if ev.Name == "add" {
			id := ev.Value
			// if !ok {
			// 	return nil, fmt.Errorf("failed to transform add event: expected value of type string, but got %s", reflect.TypeOf(ev.Value))
			// }
			evs[i].Value = string(t(id))
		}
	}
	return evs, nil
}*/
