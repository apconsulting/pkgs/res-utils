package handlers

import (
	"errors"
	"fmt"
	"net/url"
	"strings"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

var (
	ErrUnauthorized = errors.New("unauthorized")
)

type handlerOptions[T resutils.WithID] struct {
	// Gestione dei permessi
	readPerm   resutils.Permission
	createPerm resutils.Permission
	setPerm    resutils.Permission
	deletePerm resutils.Permission

	resType    res.OptionFunc
	stQHandler *store.QueryHandler[T]
	stHandler  *store.Handler[T]

	qReqHandler func(rname string, pathParams map[string]string, q url.Values) (url.Values, string, error)

	modelResPath    string
	transformer     store.Transformer[T]
	beforeTransform func(string, T) (T, error)
	afterTransform  func(string, map[string]any) (map[string]any, error)

	queryTransformer store.QueryTransformer[T]

	beforeCreateFunc func(*T) error
	afterCreateFunc  func(*T) error

	createMethod func(cr res.CallRequest)
	setMethod    func(cr res.CallRequest)
	deleteMethod func(cr res.CallRequest)

	extraMethods []extraMethod
}

type extraMethod struct {
	name string
	hndl res.Option
	perm resutils.Permission
}

type HandlerOption[T resutils.WithID] interface {
	Apply(*handlerOptions[T])
}

type HandlerOptionFunc[T resutils.WithID] func(*handlerOptions[T])

func (f HandlerOptionFunc[T]) Apply(opts *handlerOptions[T]) {
	f(opts)
}

// GESTIONE PERMESSI
// Definisce il permesso necessario per poter accedere alla risorsa in lettura
func WithReadPerm[T resutils.WithID](perm resutils.Permission) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.readPerm = perm
	})
}

// Definisce il permesso necessario per poter accedere alla risorsa in scrittura
// (creazione, modifica, eliminazione).
//
// Per impostare livelli specifici per creazione o eliminazione, usare WithCreatePerm() o
// WithDeletePerm()
func WithWritePerm[T resutils.WithID](perm resutils.Permission) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.setPerm = perm
		ho.createPerm = perm
		ho.deletePerm = perm
	})
}
func WithCreatePerm[T resutils.WithID](perm resutils.Permission) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.createPerm = perm
	})
}
func WithDeletePerm[T resutils.WithID](perm resutils.Permission) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.deletePerm = perm
	})
}

// --------------

func WithResType[T resutils.WithID](v res.OptionFunc) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.resType = v
	})
}

func WithStoreQueryHandler[T resutils.WithID](v *store.QueryHandler[T]) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.stQHandler = v
	})
}

func WithStoreHandler[T resutils.WithID](v *store.Handler[T]) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.stHandler = v
	})
}

func WithBeforeCreateFunc[T resutils.WithID](f func(*T) error) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.beforeCreateFunc = f
	})
}

func WithAfterCreateFunc[T resutils.WithID](f func(*T) error) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.afterCreateFunc = f
	})
}

func WithCreateMethod[T resutils.WithID](m func(res.CallRequest)) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.createMethod = m
	})
}

func WithSetMethod[T resutils.WithID](m func(res.CallRequest)) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.setMethod = m
	})
}

func WithDeleteMethod[T resutils.WithID](m func(res.CallRequest)) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.deleteMethod = m
	})
}

func WithTransformer[T resutils.WithID](t store.Transformer[T]) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.transformer = t
	})
}

func WithQueryTransformer[T resutils.WithID](t store.QueryTransformer[T]) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.stQHandler.Transformer = t
	})
}

func WithBeforeTransform[T resutils.WithID](f func(id string, value T) (T, error)) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.beforeTransform = f
	})
}

func WithAfterTransform[T resutils.WithID](f func(id string, parsedValue map[string]any) (map[string]any, error)) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.afterTransform = f
	})
}

func WithoutCreate[T resutils.WithID]() HandlerOption[T] {
	return WithCreateMethod[T](nil)
}
func WithoutSet[T resutils.WithID]() HandlerOption[T] {
	return WithSetMethod[T](nil)
}
func WithoutDelete[T resutils.WithID]() HandlerOption[T] {
	return WithDeleteMethod[T](nil)
}

// var WithoutCreate HandlerOptionFunc = WithCreateMethod[resutils.WithID](nil)
// var WithoutSet HandlerOptionFunc = WithSetMethod(nil)
// var WithoutDelete HandlerOptionFunc = WithDeleteMethod(nil)

// Aggiunge il call method all'handler. Se è necessario verificare un permesso specifico
// per questo metodo, indicarlo come secondo parametro. Una stringa vuota indica che
// il metodo è accessibile a tutti gli utenti che abbiano accesso in lettura all'handler
func WithMethod[T resutils.WithID](method string, permission string, f func(res.CallRequest)) HandlerOption[T] {
	if method == "set" || method == "delete" || method == "create" {
		panic(fmt.Errorf(`method "%s" is reserved`, method))
	}

	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.extraMethods = append(ho.extraMethods, extraMethod{
			name: method,
			hndl: res.Call(method, f),
			perm: resutils.Permission(permission),
		})
	})
}

func WithQueryRequestHandler[T resutils.WithID](f func(rname string, pathParams map[string]string, q url.Values) (url.Values, string, error)) HandlerOption[T] {
	return HandlerOptionFunc[T](func(ho *handlerOptions[T]) {
		ho.qReqHandler = f
	})
}

// Opzioni specifiche per un QueryHandler
type queryHandlerOptions[T resutils.WithID] handlerOptions[T]

func (qho *queryHandlerOptions[T]) ApplyToHandler(hs *res.Handler) {
	if hs.Access == nil {
		accessHandlerFactory((*handlerOptions[T])(qho)).SetOption(hs)
	}

	if qho.queryTransformer != nil {
		// Se ho settato un queryTransformer specifico, uso quello
		qho.stQHandler.Transformer = qho.queryTransformer
	} else if qho.transformer != nil {
		// Altrimenti se ho un modelTransformer, sfrutto il suo metodo IDToRID
		qho.stQHandler.Transformer = store.IDToRIDCollectionTransformer[T](func(v string) res.Ref {
			return qho.transformer.IDToRID(
				v,
				*new(T),
				res.Pattern(qho.modelResPath+".$id"),
			)
		})
		// qho.stQHandler.Transformer = store.IDToRIDCollectionTransformer[T]()
	} else {
		// Altrimenti uso un queryTransformer generico
		qho.stQHandler.Transformer = store.IDToRIDCollectionTransformer[T](func(id string) res.Ref {
			return res.Ref(strings.Join([]string{qho.modelResPath, id}, "."))
		})
	}

	qho.resType.SetOption(hs)

	if qho.qReqHandler != nil {
		qho.stQHandler.QueryRequestHandler = qho.qReqHandler
	}
	qho.stQHandler.SetOption(hs)

	if qho.createMethod != nil {
		cm := qho.createMethod

		res.Call("create", cm).SetOption(hs)
	}

	for _, method := range qho.extraMethods {
		method.hndl.SetOption(hs)
	}
}

// Opzioni specifiche per un ModelHandler
type modelHandlerOptions[T resutils.WithID] handlerOptions[T]

func (mho *modelHandlerOptions[T]) ApplyToHandler(hs *res.Handler) {
	if hs.Access == nil {
		accessHandlerFactory((*handlerOptions[T])(mho)).SetOption(hs)
	}

	mho.resType.SetOption(hs)

	if mho.transformer == nil {
		transf := DefaultTransformer[T]{}

		if mho.beforeTransform != nil {
			transf.beforeTransform = mho.beforeTransform
		}

		if mho.afterTransform != nil {
			transf.afterTransform = mho.afterTransform
		}

		mho.stHandler.Transformer = &transf
	} else {
		mho.stHandler.Transformer = mho.transformer
	}

	mho.stHandler.SetOption(hs)

	if mho.setMethod != nil {
		res.Set(mho.setMethod).SetOption(hs)
	}

	if mho.deleteMethod != nil {
		res.Call("delete", mho.deleteMethod).SetOption(hs)
	}

	for _, method := range mho.extraMethods {
		method.hndl.SetOption(hs)
	}
}

type totalHandlerOptions[T resutils.WithID] handlerOptions[T]

func (tho *totalHandlerOptions[T]) SetOptions(hs *res.Handler) {}

func accessHandlerFactory[T resutils.WithID](ho *handlerOptions[T]) res.Option {
	return res.Access(func(r res.AccessRequest) {
		var t resutils.Token

		r.ParseToken(&t)

		if !t.LoggedIn {
			r.AccessDenied()
			return
		}

		var get bool
		var calls []string = make([]string, 0)

		get = ho.readPerm == "" || resutils.HasPermission(r, ho.readPerm)

		if ho.setPerm == "" || resutils.HasPermission(r, ho.setPerm) {
			calls = append(calls, "set")
		}
		if ho.createPerm == "" || resutils.HasPermission(r, ho.createPerm) {
			calls = append(calls, "create")
		}
		if ho.deletePerm == "" || resutils.HasPermission(r, ho.deletePerm) {
			calls = append(calls, "delete")
		}

		for _, method := range ho.extraMethods {
			if method.perm == "" || resutils.HasPermission(r, method.perm) {
				calls = append(calls, method.name)
			}
		}

		r.Access(get, strings.Join(calls, ","))
	})
}
