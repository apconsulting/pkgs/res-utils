package resutils

import (
	"errors"

	"gitlab.com/apconsulting/pkgs/res/go-res"
)

var (
	ErrTxnClosed            = errors.New("txn already closed")
	ErrAssertionFailed      = errors.New("type assertion failed")
	ErrNoIDFactorySpecified = errors.New("must provide an IDFactory if the type of the id field is not ObjectID")
)

// Factory che produce un res.Error con code: "system.notFound"
func ErrNotFound(message string, data any) *res.Error {
	return &res.Error{
		Code:    "system.notFound",
		Message: message,
		Data:    data,
	}
}

// Factory che produce un res.Error con code: "system.invalidParams"
func ErrInvalidParams(message string, data any) *res.Error {
	return &res.Error{
		Code:    "system.notFound",
		Message: message,
		Data:    data,
	}
}
