package memorystore

import (
	"encoding/json"

	"github.com/mitchellh/hashstructure/v2"
	resutils "gitlab.com/apconsulting/pkgs/res-utils"
)

func HashMap[T resutils.WithID](data map[string]T) (map[string]uint64, error) {
	hashes := make(map[string]uint64)
	for key, m := range data {
		js, err := json.Marshal(m)
		if err != nil {
			return nil, err
		}

		hash, err := hashstructure.Hash(js, hashstructure.FormatV2, nil)

		if err != nil {
			return nil, err
		}

		hashes[key] = hash
	}

	return hashes, nil
}

func DataChanged[T resutils.WithID](oldData map[string]T, newData map[string]T) (bool, error) {
	// Bailout early se le mappe hanno lunghezze diverse
	if len(oldData) != len(newData) {
		return true, nil
	}

	oldHash, err := hashstructure.Hash(oldData, hashstructure.FormatV2, nil)
	if err != nil {
		return false, err
	}

	newHash, err := hashstructure.Hash(newData, hashstructure.FormatV2, nil)
	if err != nil {
		return false, err
	}

	return oldHash != newHash, nil
}
