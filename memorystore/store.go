package memorystore

import (
	"context"
	"fmt"
	"net/url"
	"sort"
	"sync"
	"time"

	"log/slog"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// MemoryStore è uno store che mantiene una mappa di dati in memoria e, tramite il metodo
// IngestNewData(), aggiorna questa mappa calcolando le differenze tra prima e dopo e
// notificando dei cambiamenti avvenuti.
//
// # Implementa sia store.Store che store.QueryStore
type MemoryStore[T resutils.WithID] struct {
	mx sync.RWMutex

	pollFunc PollFunc[T]
	data     map[string]T
	hashes   map[string]uint64

	timer       *time.Timer
	interval    time.Duration
	incremental bool

	ctx         context.Context
	ctxCancFunc context.CancelFunc

	clock         <-chan time.Time
	clockOnStopCB func()

	// closeChan   chan struct{}
	pollNowChan chan struct{}

	// destroyed atomic.Bool

	// Callbacks registrate tramite l'interfaccia store.Store
	onChangeCallbacks []func(string, store.Nullable[T], store.Nullable[T], any)

	// Callbacks registrate tramite l'interfaccia store.QueryStore
	onQueryChangeCallbacks []func(store.QueryChange[T])

	// queryFunc si occupa di filtrare il dataset in memoria usando i valori di query
	// specificati. Ritorna la lista degli id delle righe filtrate.
	queryFunc func(map[string]T, url.Values) ([]string, error)

	// createFunc è la funzione che invia la richiesta di creazione nuovo model all'API.
	createFunc func(any) error

	// updateFunc è la funzione che invia la richiesta di modifica all'API. I parametri sono
	// l'id dell'elemento da modificare e il nuovo valore
	updateFunc func(string, T) error

	// deleteFunc è la funzione che invia la richiesta di eliminazione all'API. Il parametro
	// è l'id dell'elemento da eliminare
	deleteFunc func(string) error
}

// Assert implementation
var _ resutils.CompleteStore[resutils.WithID] = (*MemoryStore[resutils.WithID])(nil)

type PollFunc[T resutils.WithID] func(ctx context.Context, prevMap map[string]T) (map[string]T, error)

// Funzione che genera un nuovo MemoryStore.
//
// pollFunc è la funzione che genera un nuovo dataset. Viene invocata inizialmente alla
// creazione del nuovo store per caricare il dataset iniziale e poi ad ogni ciclo di
// polling.
//
// interval è la frequenza con cui viene effettuato il polling all'API. Se = 0, non viene
// mai effettuato un polling automatico: fornire i dati iniziali tramite WithInitialData e
// mantenere lo store aggiornato in altro modo
func NewMemoryStore[T resutils.WithID](pollFunc PollFunc[T], interval time.Duration, opts ...StoreOption[T]) (*MemoryStore[T], error) {
	newStore := &MemoryStore[T]{}

	newStore.ctx, newStore.ctxCancFunc = context.WithCancel(context.Background())

	// Defaults
	newStore.queryFunc = func(data map[string]T, q url.Values) ([]string, error) {
		ids := make([]string, len(data))

		i := 0
		for k := range data {
			ids[i] = k
			i++
		}

		sort.Strings(ids)

		return ids, nil
	}

	newStore.createFunc = func(any) error {
		return res.ErrMethodNotFound
	}

	newStore.updateFunc = func(string, T) error {
		return res.ErrMethodNotFound
	}

	newStore.deleteFunc = func(string) error {
		return res.ErrMethodNotFound
	}

	for _, o := range opts {
		o.Apply(newStore)
	}

	ctx, cancFunc := context.WithTimeout(newStore.ctx, 3*time.Second)
	defer cancFunc()

	if newStore.data == nil {
		var err error
		newStore.data, err = pollFunc(ctx, make(map[string]T))
		if err != nil {
			return nil, err
		}
	}

	var err error
	newStore.hashes, err = HashMap(newStore.data)
	if err != nil {
		return nil, err
	}
	newStore.pollFunc = pollFunc

	newStore.OnChange(func(id string, before, after store.Nullable[T], info interface{}) {
		for _, cb := range newStore.onQueryChangeCallbacks {
			cb(&resutils.QueryChange[T]{
				Id:        id,
				BeforeVal: before,
				AfterVal:  after,
			})
		}
	})

	// Scelgo quale clock usare, quello fornito se indicato altrimenti starto un timer
	// interno (se interval > 0)
	if newStore.clock == nil {
		if interval > 0 {
			newStore.interval = interval
			newStore.timer = time.NewTimer(interval)
			newStore.clock = newStore.timer.C
		}
	}

	go func(s *MemoryStore[T]) {
		for {
			select {
			case <-s.clock:
				ctx, cancFunc := context.WithTimeout(s.ctx, 3*time.Second)

				newData, err := s.pollFunc(ctx, s.data)
				cancFunc()

				if err != nil {
					slog.Error("Errore durante il polling", "err", err)
					break
				}
				if s.incremental {
					err = s.IngestIncrementalData(newData, []string{})
					if err != nil {
						slog.Error("Errore durante il polling", "err", err)
						break
					}
				} else {
					err = s.IngestNewDataset(newData)
					if err != nil {
						slog.Error("Errore durante il polling", "err", err)
						break
					}
				}

				// Riavvio il timer se interno
				if s.timer != nil {
					s.timer.Reset(s.interval)
				}
			case <-s.pollNowChan:
				var timerStopped bool = true

				if s.timer != nil {
					timerStopped = s.timer.Stop()
				}

				if timerStopped {
					ctx, cancFunc := context.WithTimeout(s.ctx, 3*time.Second)

					newData, err := s.pollFunc(ctx, s.data)
					cancFunc()

					if err != nil {
						slog.Error("Errore durante il polling", "err", err)
						break
					}
					if s.incremental {
						err = s.IngestIncrementalData(newData, []string{})
						if err != nil {
							slog.Error("Errore durante il polling", "err", err)
							break
						}
					} else {
						err = s.IngestNewDataset(newData)
						if err != nil {
							slog.Error("Errore durante il polling", "err", err)
							break
						}
					}

					if s.timer != nil {
						s.timer.Reset(s.interval)
					}
				}
			case <-s.ctx.Done():
				if s.timer != nil {
					s.timer.Stop()
				}

				if s.clockOnStopCB != nil {
					s.clockOnStopCB()
				}

				// Libero le risorse (tecnicamente il GC dovrebbe farlo da solo, ma giusto per
				// essere scrupolosi...)
				s.data = nil
				s.hashes = nil

				return
			}
		}
	}(newStore)

	return newStore, nil
}

// Stoppa la goroutine del memorystore
func (s *MemoryStore[T]) Destroy() error {
	if s.ctx.Err() != nil {
		return fmt.Errorf("store closed: %w", s.ctx.Err())
	}

	s.ctxCancFunc()
	return nil
}

// Crea un nuovo dataset in maniera incrementale mergiando l'attuale dataset memorizzato
// nello store con newData, eventualmente eliminando le chiavi indicate in "deleted".
// Questo nuovo dataset viene poi passato a ingest() per l'emissione degli eventi.
func (s *MemoryStore[T]) IngestIncrementalData(newData map[string]T, deleted []string) error {
	s.mx.Lock()

	// Creo nuovo dataset con i dati del precedente
	newDataset := make(map[string]T, len(s.data))
	for k, v := range s.data {
		newDataset[k] = v
	}

	// Mergio newData in newDataset
	for k, v := range newData {
		newDataset[k] = v
	}

	// Elimino le chiavi presenti in deletedKeys da newDataset
	for _, k := range deleted {
		delete(newDataset, k)
	}

	// Emetto gli eventi
	return s.ingest(newDataset)
}

// Funzione che accetta un nuovo dataset, calcola le differenze col dataset precedente
// emettendo tutti gli eventi rilevati e quindi sostituisce il dataset memorizzato con il
// nuovo
func (s *MemoryStore[T]) IngestNewDataset(newData map[string]T) error {
	s.mx.Lock()
	return s.ingest(newData)
}

func (s *MemoryStore[T]) ingest(newDataset map[string]T) error {
	defer s.mx.Unlock()

	changed, err := DataChanged(s.data, newDataset)
	if err != nil {
		return err
	}

	if changed {
		newHashes, err := HashMap(newDataset)
		if err != nil {
			return err
		}

		type diff[T resutils.WithID] struct {
			resutils.QueryChange[T]
			op string
		}

		diffList := make([]diff[T], 0)

		for id, val := range newDataset {
			if oldVal, ok := s.data[id]; ok {
				// Se ho una corrispondenza tra le due mappe verifico
				if s.hashes[id] != newHashes[id] {
					// Se il dato è cambiato accodo una notifica
					diffList = append(diffList, diff[T]{
						QueryChange: resutils.QueryChange[T]{
							Id:        val.GetID(),
							BeforeVal: store.AsNullable(oldVal),
							AfterVal:  store.AsNullable(val),
						},
						op: "change",
					})
				}

				// Ho verificato il dato nel vecchio dataset, quindi lo elimino
				delete(s.data, id)
				// delete(s.hashes, id)
			} else {
				// Se il dato non c'è nella vecchia mappa, è un'aggiunta
				diffList = append(diffList, diff[T]{
					QueryChange: resutils.QueryChange[T]{
						Id:       val.GetID(),
						AfterVal: store.AsNullable(val),
					},
					op: "create",
				})
			}
		}

		// A questo punto nel vecchio dataset sono rimaste solamente le righe eliminate
		for _, val := range s.data {
			diffList = append(diffList, diff[T]{
				QueryChange: resutils.QueryChange[T]{
					Id:        val.GetID(),
					BeforeVal: store.AsNullable(val),
				},
				op: "delete",
			})
		}

		// Ho finito di verificare i dati, sostituisco il dataset coi nuovi valori
		s.data = newDataset
		s.hashes = newHashes

		for _, d := range diffList {
			// Notifico tutto
			switch d.op {
			case "change":
				s.emitChange(d.Id, d.BeforeVal, d.AfterVal)
			case "delete":
				s.emitChange(d.Id, d.BeforeVal, store.Nullable[T]{})
			case "create":
				s.emitChange(d.Id, store.Nullable[T]{}, d.AfterVal)
			}
		}
	}

	return nil
}

// Registra una callback sugli eventi riguardanti un singolo Model
func (s *MemoryStore[T]) OnChange(cb func(string, store.Nullable[T], store.Nullable[T], any)) {
	s.onChangeCallbacks = append(s.onChangeCallbacks, cb)
}

func (s *MemoryStore[T]) emitChange(id string, before, after store.Nullable[T]) {
	for _, cb := range s.onChangeCallbacks {
		cb(id, before, after, nil)
	}
}

func (s *MemoryStore[T]) pollNow() {
	// Non-blocking send on channel
	select {
	case s.pollNowChan <- struct{}{}:
	default:
	}
}

func (s *MemoryStore[T]) Read(id string) store.ReadTxn[T] {
	return s.ReadCtx(s.ctx, id)
}

func (s *MemoryStore[T]) ReadCtx(ctx context.Context, id string) store.ReadTxn[T] {
	//TODO: Usare il context
	s.mx.RLock()

	return &MemoryReadTxn[T]{
		store: s,
		id:    id,
	}
}

func (s *MemoryStore[T]) Write(id string) store.WriteTxn[T] {
	return s.WriteCtx(s.ctx, id)
}

func (s *MemoryStore[T]) WriteCtx(ctx context.Context, id string) store.WriteTxn[T] {
	//TODO: Usare il context
	s.mx.Lock()

	newTx := MemoryWriteTxn[T]{}
	newTx.id = id
	newTx.store = s

	return &newTx
}

// IMPLEMENTAZIONE DI store.QueryStore
func (s *MemoryStore[T]) OnQueryChange(cb func(store.QueryChange[T])) {
	s.onQueryChangeCallbacks = append(s.onQueryChangeCallbacks, cb)
}

func (s *MemoryStore[T]) Query(q url.Values) ([]string, error) {
	s.mx.RLock()
	defer s.mx.RUnlock()

	ids, err := s.queryFunc(s.data, q)

	if err != nil {
		return nil, err
	}

	return ids, nil
}

func (s *MemoryStore[T]) GenerateNewID(newVal T) (string, error) {
	//return "", errors.New("MemoryStore does not support creation")
	return "", nil
}

func (s *MemoryStore[T]) GetTotal(q url.Values) (int64, error) {
	if s.ctx.Err() != nil {
		return 0, fmt.Errorf("store closed: %w", s.ctx.Err())
	}

	n := len(s.data)
	return int64(n), nil
}
