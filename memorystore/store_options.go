package memorystore

import (
	"net/url"
	"time"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
)

type StoreOption[T resutils.WithID] interface {
	Apply(*MemoryStore[T])
}

type storeOptionFunc[T resutils.WithID] func(st *MemoryStore[T])

func (o storeOptionFunc[T]) Apply(st *MemoryStore[T]) {
	o(st)
}

func WithQueryFunc[T resutils.WithID](qf func(map[string]T, url.Values) ([]string, error)) StoreOption[T] {
	return storeOptionFunc[T](func(st *MemoryStore[T]) {
		st.queryFunc = qf
	})
}

func WithCreateFunc[T resutils.WithID](cf func(any) error) StoreOption[T] {
	return storeOptionFunc[T](func(st *MemoryStore[T]) {
		st.createFunc = cf
	})
}

func WithUpdateFunc[T resutils.WithID](uf func(string, T) error) StoreOption[T] {
	return storeOptionFunc[T](func(st *MemoryStore[T]) {
		st.updateFunc = uf
	})
}

func WithDeleteFunc[T resutils.WithID](df func(string) error) StoreOption[T] {
	return storeOptionFunc[T](func(st *MemoryStore[T]) {
		st.deleteFunc = df
	})
}

func WithInitialData[T resutils.WithID](data map[string]T) StoreOption[T] {
	return storeOptionFunc[T](func(st *MemoryStore[T]) {
		st.data = make(map[string]T)

		for k, v := range data {
			st.data[k] = v
		}
	})
}

func WithIncrementalPolling[T resutils.WithID]() StoreOption[T] {
	return storeOptionFunc[T](func(st *MemoryStore[T]) {
		st.incremental = true
	})
}

func WithExternalClock[T resutils.WithID](c <-chan time.Time, onStop func()) StoreOption[T] {
	return storeOptionFunc[T](func(st *MemoryStore[T]) {
		st.clock = c
		st.clockOnStopCB = onStop
	})
}
