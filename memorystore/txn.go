package memorystore

import (
	"fmt"
	"sync/atomic"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// ReadTxn
type MemoryReadTxn[T resutils.WithID] struct {
	store  *MemoryStore[T]
	id     string
	closed atomic.Bool
}

// Assert implementation
var _ store.ReadTxn[resutils.WithID] = &MemoryReadTxn[resutils.WithID]{}

func (r *MemoryReadTxn[T]) Close() error {
	if r.closed.Swap(true) {
		return resutils.ErrTxnClosed
	}

	r.store.mx.RUnlock()

	return nil
}

func (r *MemoryReadTxn[T]) Exists() bool {
	_, ok := r.store.data[r.id]
	return ok
}

func (r *MemoryReadTxn[T]) ID() string {
	return r.id
}

func (r *MemoryReadTxn[T]) Value() (T, error) {
	val, ok := r.store.data[r.id]

	if !ok {
		return val, store.ErrNotFound
	}

	// result, err := resutils.ResParse(val)
	// if err != nil {
	// 	return nil, err
	// }

	return val, nil
}

// WriteTxn
type MemoryWriteTxn[T resutils.WithID] struct {
	MemoryReadTxn[T]
}

// Assert implementation
var _ store.WriteTxn[resutils.WithID] = &MemoryWriteTxn[resutils.WithID]{}

func (w *MemoryWriteTxn[T]) Create(val *T) error {
	err := w.store.createFunc(val)
	if err != nil {
		return err
	}

	w.store.pollNow()

	return nil
}

func (w *MemoryWriteTxn[T]) Delete() error {
	err := w.store.deleteFunc(w.ID())
	if err != nil {
		return err
	}

	w.store.pollNow()

	return nil
}

func (w *MemoryWriteTxn[T]) Update(newVal T) error {
	// Assert newVal is T
	valT := newVal
	// valT, ok := newVal.(T)
	// if !ok {
	// 	return resutils.ErrAssertionFailed
	// }

	err := w.store.updateFunc(w.ID(), valT)
	if err != nil {
		return err
	}

	w.store.pollNow()

	return nil
}

func (w *MemoryWriteTxn[T]) UpdatePartial(v map[string]any) error {
	return fmt.Errorf("unimplemented")
}

// Reimplemento il metodo Close perché quello di readTxn embeddato fa una RUnlock(), ma in
// realtà sto detenendo un Lock di scrittura e quindi devo usare Unlock()
func (w *MemoryWriteTxn[T]) Close() error {
	if w.closed.Swap(true) {
		return resutils.ErrTxnClosed
	}

	w.store.mx.Unlock()

	return nil
}
