package resutils

import (
	"encoding/base64"
	"fmt"
	"net/url"
	"reflect"
	"regexp"
	"sort"
	"strings"
	"time"

	"gitlab.com/apconsulting/pkgs/res/go-res"
	"gitlab.com/apconsulting/pkgs/res/go-res/resprot"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Token struct {
	LoggedIn bool     `json:"logged_in"`
	ID       string   `json:"oid"`
	Perms    []string `json:"perms" res:"data"`
}

// Questo tipo serve come chiave per il context.WithValue, cosi da evitare conflitti con
// altri package che usano context
type CID struct{}

type EventInfo struct {
	Initiator string `json:"initiator,omitempty"`
}

type Permission string

type withParseToken interface {
	ParseToken(interface{})
}

// Verifica che chi richiede la risorsa abbia una sessione valida attiva
var CheckLoggedIn = res.Access(func(r res.AccessRequest) {
	var t Token

	r.ParseToken(&t)

	if t.LoggedIn {
		r.AccessGranted()
	} else {
		r.AccessDenied()
	}
})

func HasPermission(r withParseToken, perm Permission) bool {
	var t Token
	r.ParseToken(&t)
	return hasPerm(t, perm)
}

func hasPerm(t Token, perm Permission) bool {
	found := false
	for _, p := range t.Perms {
		if p == string(perm) || p == "*" {
			found = true
			break
		}
	}

	return found
}

func CheckPermission(perm Permission) res.Option {
	return res.Access(func(r res.AccessRequest) {
		var t Token

		r.ParseToken(&t)

		if !t.LoggedIn {
			r.AccessDenied()
			return
		}

		if !hasPerm(t, perm) {
			r.AccessDenied()
			return
		}

		r.AccessGranted()
	})
}

// Parsa una struttura v cercando tag "res" nei campi e applicando la logica opportuna. Il
// valore di ritorno è una map[string]any. La funzione va in errore se v non è struct,
// puntatore a struct o interfaccia il cui valore dinamico è una struct.
//
// I tag dispnibili sono:
//
// - `res:"data:{key}"` embedda il valore originale del campo in un oggetto res.DataValue
// e lo assegna alla chiave "key" nella mappa di ritorno
//
// - `res:"rid:{subject.$(idfield,opts)}"` genera un rid per la risorsa, sostituendo il
// parametro $idfield con il valore del campo "idField" della struttura stessa, applicando
// le eventuali opzioni indicate in opts. NB: Il tipo del campo taggato `res:rid` è
// ininfluente, nella mappa finale si otterrà sempre una variabile di tipo res.Ref.
// Es per la struttura:
//
//	var S struct {
//		MyID int
//		External string `res:"rid:ms_test.method.$(MyID)"`
//	}{
//		MyID: 150,
//	}
//
// il valore finale di "External" nella mappa sarà:
//
//	"External": { "rid": "ms_test.method.150" }
//
// Le possibili opts sono:
//
//	base64: effettua l'encoding in base64 del valore di idfield prima di compilare il rid
//
// - `res:"soft:{subject.$(idfield,opts)}` è equivalente all'opzione "rid" ma il valore
// generato sarà di tipo res.SoftRef
func ResParse(v any) (map[string]any, error) {
	if v == nil {
		return nil, nil
	}

	resultMap := make(map[string]any)

	inVal := reflect.ValueOf(v)
	for inVal.Kind() == reflect.Pointer || inVal.Kind() == reflect.Interface {
		inVal = inVal.Elem()
	}

	if inVal.Kind() != reflect.Struct {
		return nil, fmt.Errorf("supplied value must be a struct, pointer to struct or interface boxing a struct")
	}

	inType := reflect.TypeOf(inVal.Interface())

structFieldsLoop:
	for i := 0; i < inType.NumField(); i++ {
		field := inType.Field(i)
		fieldValue := inVal.Field(i)

		if field.Anonymous {
			continue
		}

		mapKey := field.Name
		if tag, ok := field.Tag.Lookup("json"); ok {
			//Tag json trovato
			a, _, _ := strings.Cut(tag, ",")
			if a == "-" {
				continue
			}
			mapKey = a
		}

		if tag, ok := field.Tag.Lookup("res"); ok {
			splits := strings.Split(tag, ":")

			switch splits[0] {
			case "data":
				opVal := mapKey
				if len(splits) > 1 {
					opVal = splits[1]
				}

				resultMap[opVal] = res.DataValue{
					Data: fieldValue.Interface(),
				}
			case "rid", "soft":
				if len(splits) < 2 {
					return nil, fmt.Errorf(`not enough parameters in res:rid tag`)
				}

				opVal := splits[1]

				if field.Type.Kind() == reflect.Pointer {
					if fieldValue.IsNil() {
						continue structFieldsLoop
					}
					v = fieldValue.Elem()
				}

				rx := regexp.MustCompile(`\$\(([^\.]*)\)`)
				parts := rx.FindAllStringSubmatch(opVal, -1)

				var parsedRid string = opVal

				for j := 0; j < len(parts); j++ {
					idFieldNameOpts := parts[j][1]

					idFieldName, idFieldOpts, hasOpts := strings.Cut(idFieldNameOpts, ",")
					idField, ok := inType.FieldByName(idFieldName)
					idFieldType := idField.Type

					if !ok {
						return nil, fmt.Errorf(`id field "%s" not found while parsing res:rid tag for field "%s"`, idFieldName, field.Name)
					}

					idFieldVal := inVal.FieldByName(idFieldName)

					// Genero una mappa delle opzioni attivate per facilitare il confronto più giù
					opts := make(map[string]struct{})
					if hasOpts {
						optsSplits := strings.Split(idFieldOpts, ",")
						for _, opt := range optsSplits {
							opts[opt] = struct{}{}
						}
					}

					if idFieldVal.Kind() == reflect.Pointer {
						// Se il valore è un puntatore e il puntatore è nil, skippo la generazione del
						//rid (vd. opazione omitempty)
						if idFieldVal.IsNil() {
							continue structFieldsLoop
						}

						idFieldVal = idFieldVal.Elem()
						idFieldType = reflect.TypeOf(idFieldVal.Interface())
					}

					// Se uno qualsiasi degli idField ha l'opzione omitempty e il relativo
					// idFieldValue è zero, skippo completamente la generazione del rid in quanto
					// non sarebbe un rid valido (ad es. se idField è una stringa e il suo valore
					// è stringa vuota, il rid generato non potrebbe essere valido in nessun caso)
					if _, ok := opts["omitempty"]; ok && idFieldVal.IsZero() {
						continue structFieldsLoop
					}

					// Mi salvo il reflect.Type di un ObjectID per fare i confronti
					var oidType = reflect.TypeOf(primitive.ObjectID{})

					if idFieldType.Kind() != reflect.String && idFieldType.Kind() != reflect.Int && idFieldType != oidType {
						return nil, fmt.Errorf(`res:rid referenced field "%s" must be string, int, ObjectID or a pointer to one of those`, idFieldName)
					}

					parsedRid = strings.Replace(parsedRid, "$("+idFieldNameOpts+")", "%v", 1)

					var idFieldStringVal string
					if idFieldType == oidType {
						idFieldStringVal = idFieldVal.Interface().(primitive.ObjectID).Hex()
					} else {
						idFieldStringVal = fmt.Sprintf("%v", idFieldVal)
					}

					// Se l'opzione base64 è attiva, codifico il valore in base64
					if _, ok := opts["base64"]; ok {
						idFieldStringVal = base64.URLEncoding.EncodeToString([]byte(idFieldStringVal))
					}

					parsedRid = fmt.Sprintf(parsedRid, idFieldStringVal)
				}

				if splits[0] == "rid" {
					rid := res.Ref(parsedRid)
					if !rid.IsValid() {
						return nil, fmt.Errorf(`res:rid the generated rid is not valid "%s"`, rid)
					}

					resultMap[mapKey] = rid
				} else {
					softRid := res.SoftRef(parsedRid)
					if !softRid.IsValid() {
						return nil, fmt.Errorf(`res:soft the generated soft rid is not valid "%s"`, softRid)
					}

					resultMap[mapKey] = softRid
				}
			}
		} else {
			resultMap[mapKey] = fieldValue.Interface()
		}
	}

	return resultMap, nil
}

// ResUnparse compila una mappa simile a quella fornita, intercettando i valori di tipo
// res.DataValue e sostituendoli con il valore della loro proprietà Data (in pratica
// disfacendo ciò che ResParse fa ai campi res:"data")
func ResUnparse(m map[string]any) map[string]any {
	var resultMap = make(map[string]any)
	// dataValueType := reflect.TypeOf(res.DataValue{})

	// for k, v := range val {
	// 	if reflect.TypeOf(v) == dataValueType {
	// 		assertedVal := v.(res.DataValue)
	// 		resultMap[k] = assertedVal.Data
	// 	} else {
	// 		resultMap[k] = v
	// 	}
	// }

	for k, v := range m {
		switch val := v.(type) {
		case res.DataValue:
			resultMap[k] = val.Data
		default:
			resultMap[k] = val
		}
	}

	return resultMap
}

// SendRequestCollection invia la richiesta su protocollo RES e risolve contestualmente
// tutti i RID, ritornando la lista di valori
func SendRequestCollection[T any](nc res.Conn, subject string, req interface{}, timeout time.Duration, onTimeoutExtend ...func(time.Duration)) ([]T, error) {
	response := resprot.SendRequest(nc, subject, nil, time.Second, onTimeoutExtend...)
	if response.Error != nil {
		return nil, response.Error
	}

	listRid := make([]res.Ref, 0)
	_, err := response.ParseCollection(&listRid)
	if err != nil {
		return nil, err
	}

	listUl := make([]T, 0)
	for _, rid := range listRid {
		tmpT, err := SendRequestModel[T](nc, fmt.Sprintf("get.%s", string(rid)), req, timeout, onTimeoutExtend...)
		if err != nil {
			return nil, err
		}
		listUl = append(listUl, tmpT)
	}
	return listUl, nil
}

// SendRequestModel invia la richiesta su protocollo RES e ritorna il modello
func SendRequestModel[T any](nc res.Conn, subject string, req interface{}, timeout time.Duration, onTimeoutExtend ...func(time.Duration)) (T, error) {
	tmpT := *new(T)
	res := resprot.SendRequest(nc, subject, nil, time.Second, onTimeoutExtend...)
	if res.Error != nil {
		return tmpT, res.Error
	}
	_, err := res.ParseModel(&tmpT)
	return tmpT, err
}

// QueryRequestHandler è una funzione che, presa una query proveniente da una querystring,
// ne tratta i valori e ne crea una versione "normalizzata", ovvero se date due query
// queste sono tra di loro equivalenti (ad es. se cambia solamente l'ordine in cui
// compaiono i parametri nella querystring), questa funzione deve ritornare la stessa
// query normalizzata.
//
// Questo serve a permettere a Resgate di riconoscere quali query fanno in realtà
// riferimento agli stessi dati, ottimizzando così il processo di caching.
//
// Questo default aggiunge sempre il parametro "from" in testa e accoda tutti gli altri
// parametri in ordine alfabetico
func DefaultQueryRequestHandler(rname string, pathParams map[string]string, q url.Values) (url.Values, string, error) {
	from := q.Get("from")
	if from == "" {
		q.Set("from", "0")
	}

	list := make([]string, 0)
	for k := range q {
		list = append(list, k)
	}

	sort.Strings(list)

	for i, k := range list {
		if value := q.Get(k); value != "" {
			list[i] = fmt.Sprintf("%s=%v", k, value)
		}
	}

	normalizedQuery := strings.Join(list, "&")
	return q, normalizedQuery, nil
}

// // Mergia i valori di b in a, facendo avanti e indietro dalla rappresentazione JSON di
// // entrambi
// func Merge[T WithID](a T, b map[string]any) (T, error) {
// 	var newVal T

// 	aJs, err := json.Marshal(a)
// 	if err != nil {
// 		return newVal, err
// 	}

// 	aMap := make(map[string]any)
// 	err = json.Unmarshal(aJs, &aMap)
// 	if err != nil {
// 		return newVal, err
// 	}

// 	for k, v := range b {
// 		aMap[k] = v
// 	}

// 	aJs, err = json.Marshal(aMap)
// 	if err != nil {
// 		return a, err
// 	}

// 	err = json.Unmarshal(aJs, &newVal)
// 	if err != nil {
// 		return newVal, err
// 	}

// 	return newVal, nil
// }
