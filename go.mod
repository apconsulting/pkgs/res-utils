module gitlab.com/apconsulting/pkgs/res-utils

go 1.22

toolchain go1.22.3

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/mitchellh/hashstructure/v2 v2.0.2
	github.com/nats-io/nats.go v1.35.0
	github.com/stretchr/testify v1.8.4
	gitlab.com/apconsulting/pkgs/res/go-res v0.4.9
	go.mongodb.org/mongo-driver v1.15.0
)

require (
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/jirenius/timerqueue v1.0.0 // indirect
	github.com/klauspost/compress v1.17.8 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/nats-io/nats-server/v2 v2.10.5 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	golang.org/x/crypto v0.23.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
