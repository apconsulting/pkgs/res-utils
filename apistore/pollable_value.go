package apistore

import (
	"encoding/json"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"github.com/mitchellh/hashstructure/v2"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

type PollableValue[T any] struct {
	mx sync.RWMutex

	ID    string
	Value T
	Hash  uint64

	timeout       *time.Timer
	pollingTicker *time.Ticker

	running   atomic.Bool
	exitChan  chan struct{}
	clearFunc func()
}

func NewPollableValue[T any](id string) *PollableValue[T] {
	return &PollableValue[T]{
		ID:       id,
		exitChan: make(chan struct{}),
	}
}

func (v *PollableValue[T]) StartPolling(getNewValue func() (T, error), clearFunc func(), emitChange func(string, store.Nullable[T], store.Nullable[T])) error {
	v.mx.Lock()
	defer v.mx.Unlock()

	// Carico il valore iniziale
	startingVal, queryErr := getNewValue()
	if queryErr == store.ErrNotFound {
		return store.ErrNotFound
	} else if queryErr != nil {
		return queryErr
	}

	v.clearFunc = clearFunc

	// Polla per 10 secondi, poi chiama clearFunc
	v.timeout = time.AfterFunc(time.Second*10, func() {
		v.Destroy()
	})

	// Polla ogni secondo
	v.pollingTicker = time.NewTicker(time.Second)

	v.Value = startingVal
	js, err := json.Marshal(startingVal)

	if err != nil {
		return err
	}

	v.Hash, err = hashstructure.Hash(js, hashstructure.FormatV2, nil)
	if err != nil {
		return err
	}

	// Avvio la goroutine di polling
	go func() {
		v.running.Store(true)

		defer v.Destroy()

		// fmt.Println("Polling per", v.ID, "iniziato")
		fmt.Println(v.ID, " --> Polling started")

		for {
			select {
			case <-v.exitChan:
				return
			case <-v.pollingTicker.C:
				// Chiudo tutto in una func per poter usare il defer e magari gestire i panic
				func() {
					v.mx.Lock()
					defer v.mx.Unlock()

					// Carico il nuovo valore
					newVal, queryErr := getNewValue()

					// Se non esiste, allora è stato eliminato
					if queryErr == store.ErrNotFound {
						// Emetto e stoppo tutto

						emitChange(v.ID, store.AsNullable(v.Value), store.Nullable[T]{})

						return
					}

					// Il valore esiste ancora, vedo se è cambiato
					js, err := json.Marshal(newVal)
					if err != nil {
						panic(err) // TODO: Gestire errori
					}

					newHash, err := hashstructure.Hash(js, hashstructure.FormatV2, nil)
					if err != nil {
						panic(err) // TODO: Gestire errori
					}

					if v.Hash != newHash {
						// Il valore è cambiato, lo salvo e notifico
						oldValue := v.Value
						v.Hash = newHash
						v.Value = newVal
						fmt.Println(v.ID, " -->> CHANGED:", oldValue, v.Value)
						emitChange(v.ID, store.AsNullable[T](oldValue), store.AsNullable(v.Value))
					}
				}()
			}
		}
	}()

	return nil
}

func (v *PollableValue[T]) Destroy() error {
	if v.running.Swap(false) {
		// fmt.Println("Polling per", v.ID, "distrutto!")
		fmt.Println(v.ID, " --X Destroyed")
		v.timeout.Stop()
		v.pollingTicker.Stop()
		close(v.exitChan)
		v.clearFunc()
	}

	return nil
}

func (v *PollableValue[T]) Set(val T) error {
	v.mx.Lock()
	defer v.mx.Unlock()

	v.Value = val

	return nil
}

func (v *PollableValue[T]) Get() T {
	v.mx.RLock()
	defer v.mx.RUnlock()

	return v.Value
}
