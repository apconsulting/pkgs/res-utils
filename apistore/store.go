package apistore

import (
	"fmt"
	"net/url"
	"sync"
	"sync/atomic"

	resutils "gitlab.com/apconsulting/pkgs/res-utils"
	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

func NewApiStore[T resutils.WithID]() *ApiStore[T] {
	newStore := &ApiStore[T]{}
	newStore.collections = make(map[string]*PollableValue[[]string])
	newStore.models = make(map[string]*PollableValue[T])

	newStore.OnChange(func(id string, before, after store.Nullable[T], info interface{}) {
		// var a, b T

		// if before != nil {
		// 	b = before.(T)
		// }
		// if after != nil {
		// 	a = after.(T)
		// }
		for _, cb := range newStore.onQueryChangeCallbacks {
			cb(&ApiQueryChange[T]{
				id:     id,
				before: before,
				after:  after,
			})
		}
	})

	return newStore
}

// Store che pone sotto polling ogni risorsa che gli viene richiesta per un intervallo di
// tempo prefissato rendendolo così reattivo. Passato questo tempo emette un reset per la
// risorsa, notificando l'impossibilità di mantenere la reattività. Un eventuale resgate
// in ascolto effettuerà quindi una nuova sottoscrizione, se ancora necessaria. E'
// un'alternativa granulare del memorystore utile quando non è possibile definire a priori
// le risorse da osservare (ad es. slab in easyapi)
type ApiStore[T resutils.WithID] struct {
	// Callbacks registrate tramite l'interfaccia store.Store
	onChangeCallbacks []func(string, store.Nullable[T], store.Nullable[T], any)

	// Callbacks registrate tramite l'interfaccia store.QueryStore
	onQueryChangeCallbacks []func(store.QueryChange[T])

	mx sync.RWMutex

	models      map[string]*PollableValue[T]
	collections map[string]*PollableValue[[]string]

	GetModelFunc func(id string) func() (T, error)
	QueryFunc    func(q url.Values) func() ([]string, error)

	ResetModelFunc func(id string) error
	ResetQueryFunc func() error
}

// Assert implementation
var _ store.Store[resutils.WithID] = &ApiStore[resutils.WithID]{}
var _ store.QueryStore[resutils.WithID] = &ApiStore[resutils.WithID]{}

func (s *ApiStore[T]) Read(id string) store.ReadTxn[T] {
	s.mx.Lock()

	return &ApiReadTxn[T]{
		store: s,
		id:    id,
	}
}

func (s *ApiStore[T]) Write(id string) store.WriteTxn[T] {
	s.mx.Lock()

	newTx := ApiWriteTxn[T]{}
	newTx.id = id
	newTx.store = s

	return &newTx
}

func (s *ApiStore[T]) OnChange(cb func(string, store.Nullable[T], store.Nullable[T], any)) {
	s.onChangeCallbacks = append(s.onChangeCallbacks, cb)
}

func (s *ApiStore[T]) EmitChange(id string, before, after store.Nullable[T]) {
	for _, cb := range s.onChangeCallbacks {
		cb(id, before, after, nil)
	}
}

// ReadTxn
type ApiReadTxn[T resutils.WithID] struct {
	store  *ApiStore[T]
	id     string
	closed atomic.Bool
}

// Assert implementation
var _ store.ReadTxn[resutils.WithID] = &ApiReadTxn[resutils.WithID]{}

func (r *ApiReadTxn[T]) Close() error {
	if r.closed.Swap(true) {
		return resutils.ErrTxnClosed
	}

	r.store.mx.Unlock()

	return nil
}

func (r *ApiReadTxn[T]) Exists() bool {
	_, ok := r.store.models[r.id]
	return ok
}

func (r *ApiReadTxn[T]) ID() string {
	return r.id
}

func (r *ApiReadTxn[T]) Value() (T, error) {
	val, ok := r.store.models[r.id]

	// fmt.Println("Richiesto modello per", r.id)

	if ok {
		fmt.Println(r.id, " --> CACHED")
		return val.Value, nil
	}

	// fmt.Println("Non trovato, la creo")
	// Sto richiedendo il valore per la prima volta all'API, metto in piedi il polling
	newVal := NewPollableValue[T](r.id)
	err := newVal.StartPolling(
		r.store.GetModelFunc(r.id),
		func() {
			r.store.mx.Lock()
			defer r.store.mx.Unlock()

			delete(r.store.models, r.id)

			r.store.ResetModelFunc(r.id)
		},
		r.store.EmitChange,
	)

	if err != nil {
		return newVal.Value, err
	}

	r.store.models[r.id] = newVal

	return newVal.Value, nil
}

// WriteTxn
type ApiWriteTxn[T resutils.WithID] struct {
	ApiReadTxn[T]
}

// Assert implementation
var _ store.WriteTxn[resutils.WithID] = &ApiWriteTxn[resutils.WithID]{}

func (w *ApiWriteTxn[T]) Create(val *T) error {
	if w.id == "" {
		return fmt.Errorf("id can't be empty")
	}

	if _, ok := w.store.models[w.id]; ok {
		return store.ErrDuplicate
	}

	// Qua andiamo all'API e gestiamo errori eventuali
	// err := APICreaNuovoElemento(val)
	// if err != nil {
	// 	return err
	// }

	// if (elemento creato) {
	w.store.EmitChange(w.id, store.Nullable[T]{}, store.AsNullable(*val))
	//}

	return nil
}

func (w *ApiWriteTxn[T]) Delete() error {
	val, ok := w.store.models[w.id]

	if !ok || w.id == "" {
		return store.ErrNotFound
	}

	val.Destroy()
	delete(w.store.models, w.id)

	// Qua andiamo all'API e gestiamo errori eventuali
	// err := APIEliminaElemento(val)
	// if err != nil {
	// 	return err
	// }

	// if (elemento eliminato) {
	w.store.EmitChange(w.id, store.AsNullable(val.Value), store.Nullable[T]{})
	//}

	return nil
}

func (w *ApiWriteTxn[T]) Update(newVal T) error {
	mapVal, ok := w.store.models[w.id]

	if !ok || w.id == "" {
		return store.ErrNotFound
	}

	// assertedVal, ok := newVal.(T)
	// if !ok {
	// 	return resutils.ErrAssertionFailed
	// }

	// Qua andiamo all'API e gestiamo errori eventuali
	// err := APIModificaElemento(val)

	// if err != nil {
	// 	return err
	// }

	previousValue := mapVal.Get()
	err := mapVal.Set(newVal)
	if err != nil {
		return err
	}

	w.store.EmitChange(w.id, store.AsNullable(previousValue), store.AsNullable(newVal))

	return nil
}

func (w *ApiWriteTxn[T]) UpdatePartial(newVal map[string]any) error {
	return fmt.Errorf("unimplemented")
}

// Reimplemento il metodo Close perché quello di readTxn embeddato fa una RUnlock(), ma in
// realtà sto detenendo un Lock di scrittura e quindi devo usare Unlock()
func (w *ApiWriteTxn[T]) Close() error {
	if w.closed.Swap(true) {
		return resutils.ErrTxnClosed
	}

	w.store.mx.Unlock()

	return nil
}

// IMPLEMENTAZIONE DI store.QueryStore
func (s *ApiStore[T]) OnQueryChange(cb func(store.QueryChange[T])) {
	s.onQueryChangeCallbacks = append(s.onQueryChangeCallbacks, cb)
}

func (s *ApiStore[T]) Query(q url.Values) ([]string, error) {
	s.mx.Lock()
	defer s.mx.Unlock()

	// La collection specifica di una query è in realtà un PollableValue di []string che ha
	// come ID la rappresentazione testuale della Query
	id := q.Encode()

	if val, ok := s.collections[id]; ok {
		// Sto già pollando la query
		fmt.Println(id, " --> CACHED")
		return val.Value, nil
	}

	newCollection := NewPollableValue[[]string](q.Encode())
	s.collections[id] = newCollection

	err := newCollection.StartPolling(
		s.QueryFunc(q),
		func() {
			s.mx.Lock()
			defer s.mx.Unlock()

			delete(s.collections, id)

			// TODO: Fare la reset anche per i models
			s.ResetQueryFunc()
		},
		func(string, store.Nullable[[]string], store.Nullable[[]string]) {
			for _, cb := range s.onQueryChangeCallbacks {
				cb(&ApiQueryChange[T]{})
			}
		},
	)

	if err != nil {
		return nil, err
	}

	return newCollection.Value, nil

	// ids := make([]string, 0)

	// for _, v := range s.data {
	// 	ids = append(ids, v.GetID())
	// }

	// return ids, nil
}

type ApiQueryChange[T resutils.WithID] struct {
	id     string
	before store.Nullable[T]
	after  store.Nullable[T]
}

func (qc *ApiQueryChange[T]) ID() string {
	return qc.id
}

func (qc *ApiQueryChange[T]) Before() store.Nullable[T] {
	return qc.before
}

func (qc *ApiQueryChange[T]) After() store.Nullable[T] {
	return qc.after
}

func (qc *ApiQueryChange[T]) Events(q url.Values) (events []store.ResultEvent[T], reset bool, err error) {
	return nil, true, nil
}
