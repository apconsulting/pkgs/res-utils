package resutils

import (
	"net/url"

	"gitlab.com/apconsulting/pkgs/res/go-res/store"
)

// QueryChange represents a change to a resource that may affects queries.
type QueryChange[T any] struct {
	Id        string
	BeforeVal store.Nullable[T]
	AfterVal  store.Nullable[T]
}

func (qc *QueryChange[T]) ID() string {
	return qc.Id
}

func (qc *QueryChange[T]) Before() store.Nullable[T] {
	return qc.BeforeVal
}

func (qc *QueryChange[T]) After() store.Nullable[T] {
	return qc.AfterVal

}
func (qc *QueryChange[T]) Events(q url.Values) (events []store.ResultEvent[T], reset bool, err error) {
	return nil, true, nil
}
